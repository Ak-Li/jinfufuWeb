/**
 * Created by Administrator on 2018/6/2.
 */
import {
  USER_TOKEN,
  UPDATE_HEAD,
  UPDATE_LOADING,
  UPDATE_FOOTER,
  UPDATE_PAGE_TITLE
} from './data'

const state = {
  userToken: '',
  headShow: true,
  loading: false,
  footerShow: true,
  pageTitle: '金服服成就未来'
}

const mutations = {
  /* token */
  [USER_TOKEN] (state, type) {
    state.userToken = type
  },

  /* head */
  [UPDATE_HEAD] (state, type) {
    state.headShow = type
  },
  /* loading */
  [UPDATE_LOADING] (state, type) {
    state.loading = type
  },
  /* footer */
  [UPDATE_FOOTER] (state, type) {
    state.footerShow = type
  },
  /* title */
  [UPDATE_PAGE_TITLE] (state, type) {
    state.pageTitle = type
  }
}

const getters = {
  userToken (state) {
    return state.userToken
  },
  headShow (state) {
    return state.headShow
  },
  loading (state) {
    return state.loading
  },
  footerShow (state) {
    return state.footerShow
  },
  pageTitle (state) {
    return state.pageTitle
  }
}

export default {
  state,
  mutations,
  getters
}
