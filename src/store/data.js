/**
 * Created by Administrator on 2018/6/2.
 */
// 用户登录成功的Token
export const USER_TOKEN = 'USER_TOKEN'

// 是否显示头部
export const UPDATE_HEAD = 'UPDATE_HEAD'

// 是否显示loading
export const UPDATE_LOADING = 'UPDATE_LOADING'

// 是否显示footer
export const UPDATE_FOOTER = 'UPDATE_FOOTER'

// 页面标题
export const UPDATE_PAGE_TITLE = 'UPDATE_PAGE_TITLE'
