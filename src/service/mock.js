/**
 * Created by Administrator on 2018/6/4.
 */
const Mock = require('mockjs')
Mock.mock('/api/user/loginByCode', 'post', {
  'code': 1,
  'msg': '',
  'data': {
    'token': '1234567890',
    'hasPwd': '1'
  }
})
Mock.mock('/api/user/loginByPwd', 'post', {
  'code': 1,
  'msg': '',
  'data': {
    'token': '1234567890'
  }
})
Mock.mock('/api/user/sendVerificationCode', 'post', {
  'code': 1234,
  'msg': '',
  'data': {}
})
Mock.mock('/api/user/setPwd', 'post', {
  'code': 1,
  'msg': '',
  'data': {}
})
Mock.mock('/api/user/setHeadImg', 'post', {
  'code': 1,
  'msg': '',
  'data': {}
})
Mock.mock('/api/user/getUserInfo', 'post', {
  'code': 1,
  'msg': '',
  'data': {
    'tel': '11011110000',
    'headImg': '',
    'score': ''
  }
})
Mock.mock('/api/main/loanList', 'post', {
  'code': 1,
  'msg': '',
  'data': {
    // 1线上贷款 2线下贷款 3信用卡
    'type': '1',
    // 贷款名称
    'name': '贷款1',
    'url': 'www.baidu.com'
  }
})
Mock.mock('/api/main/loanDetail', 'post', {
  'code': 1,
  'msg': '',
  'data': {
    'img': 'https://www.baidu.com/img/bd_logo1.png',
    'url': 'www.baidu.com'
  }
})
Mock.mock('/api/main/addressList', 'post', {
  'code': 1,
  'msg': '',
  'data': [{
    'id': '1',
    'name': '江苏省'
  }, {
    'id': '2',
    'name': '浙江省'
  }]
})
Mock.mock('/api/main/storeList', 'post', {
  'code': 1,
  'msg': '',
  'data': [{
    'tel': '1111111111111',
    'name': '南京市xxxxxxx',
    'detail': '南京市xxxxxxx'
  }, {
    'tel': '122222222222222',
    'name': '南京市yyyyyy',
    'detail': '南京市xxxxxxx'
  }]
})
