/**
 * Created by Administrator on 2018/6/4.
 */
import $http from './service'
// 登录
export const ApiLoginByCode = (req) => $http.Api('/api/user/loginByCode', req)
export const ApiLoginByPwd = (req) => $http.Api('/api/user/loginByPwd', req)
export const ApiSendVerificationCode = (phoneNumber) => $http.Api('/api/user/sendVerificationCode', phoneNumber)
export const ApiSetPwd = (req) => $http.Api('/api/user/setPwd', req)
export const ApiSetHeadImg = (req) => $http.Api('/api/user/setHeadImg', req)
export const ApiGetUserInfo = (req) => $http.Api('/api/user/getUserInfo', req)

// 主页
export const ApiLoanList = (req) => $http.Api('/api/main/loanList', req)
export const ApiLoanDetail = (req) => $http.Api('/api/main/loanDetail', req)
// 门店
export const ApiAddressList = (req) => $http.Api('/api/main/addressList', req)
export const ApiStoreList = (req) => $http.Api('/api/main/storeList', req)
// 提升额度
export const ApiGetBaseInfo = (req) => $http.Api('/api/info/getBaseInfo', req)
export const ApiSetBaseInfo = (req) => $http.Api('/api/info/setBaseInfo', req)
export const ApiGetWorkInfo = (req) => $http.Api('/api/info/getWorkInfo', req)
export const ApiSetWorkInfo = (req) => $http.Api('/api/info/setWorkInfo', req)
export const ApiGetAssetsInfo = (req) => $http.Api('/api/info/getAssetsInfo', req)
export const ApiSetAssetsInfo = (req) => $http.Api('/api/info/setAssetsInfo', req)
export const ApiGetLoanInfo = (req) => $http.Api('/api/info/getLoanInfo', req)
export const ApiSetLoanInfo = (req) => $http.Api('/api/info/setLoanInfo', req)
